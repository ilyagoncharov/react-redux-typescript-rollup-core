﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Core.DAL.Models
{
    public class RefreshToken
    {
        public string Id { get; set; }
        public DateTime ExpiresUtc { get; set; }
    }
}
