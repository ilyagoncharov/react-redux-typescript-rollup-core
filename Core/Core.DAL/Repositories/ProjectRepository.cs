﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.DAL.Models;

namespace Core.DAL.Repositories
{
    public class ProjectRepository
    {
        private CoreContext Context { get; }

        public ProjectRepository(CoreContext context)
        {
            Context = context;
        }

        public List<Projects> List()
        {
            return Context.Projects.ToList();
        }
    }
}
