﻿using System;
using System.Linq;
using Core.DAL.Models;

namespace Core.DAL.Repositories
{
    public class RefreshTokenRepository
    {
        private CoreContext Context { get; }

        public RefreshTokenRepository(CoreContext context)
        {
            Context = context;
        }
        
        public RefreshToken GetById(string id)
        {
            var entity = Context.RefreshTokens.FirstOrDefault(x => x.Id == id);

            return CheckExpires(entity);
        }

        public void Add(RefreshToken token)
        {
            Context.RefreshTokens.Add(token);

            Context.SaveChanges();
        }

        private RefreshToken CheckExpires(RefreshToken entity)
        {
            if (entity?.ExpiresUtc < DateTime.UtcNow)
            {
                Context.RefreshTokens.Remove(entity);

                Context.SaveChanges();

                return null;
            }
            else
            {
                return entity;
            }
        }
    }
}
