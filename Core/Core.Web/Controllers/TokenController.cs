﻿using System;
using Core.Web.Models;
using Core.Web.Providers;
using Microsoft.AspNetCore.Mvc;

namespace Core.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/token")]
    public class TokenController : Controller
    {
        private OAuthProvider OAuthProvider { get; }

        public TokenController(OAuthProvider oAuthProvider)
        {
            OAuthProvider = oAuthProvider;
        }

        [HttpPost]
        public OperationResult Token([FromBody] JWTRequest parameters)
        {
            //System.Threading.Thread.Sleep(4000);

            if (parameters == null)
            {
                return new OperationResult
                {
                    Success = false,
                    Message = "null of parameters"
                };
            }

            switch (parameters.grant_type)
            {
                case "password":
                {
                    return OAuthProvider.DoPassword(parameters);
                }
                case "refresh_token":
                {
                    return OAuthProvider.DoRefreshToken(parameters);
                }
                default:
                    return new OperationResult
                    {
                        Success = false,
                        Message = "bad request"
                    };
            }
        }
    }
}