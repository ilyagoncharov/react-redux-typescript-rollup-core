﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.DAL.Models;
using Core.DAL.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Core.Web.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/data")]
    public class DataController : Controller
    {
        private UserRepository UserRepository { get; }
        private ProjectRepository ProjectRepository { get; }

        public DataController(UserRepository userRepository, ProjectRepository projectRepository)
        {
            UserRepository = userRepository;
            ProjectRepository = projectRepository;
        }

        [HttpGet]
        [Route("users")]
        public List<User> Users()
        {
            System.Threading.Thread.Sleep(2000);
            return UserRepository.List();
        }

        [HttpGet]
        [Route("user")]
        public User UserById(int id)
        {
            System.Threading.Thread.Sleep(2000);
            return UserRepository.GetById(id);
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("projects")]
        public List<Projects> Projects()
        {
            System.Threading.Thread.Sleep(3000);
            return ProjectRepository.List();
        }
    }
}