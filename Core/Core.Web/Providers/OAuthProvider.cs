﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Core.Common.AppSettings;
using Core.DAL.Models;
using Core.DAL.Repositories;
using Core.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.IdentityModel.Tokens;

namespace Core.Web.Providers
{
    public class OAuthProvider
    {
        private IAppConfigurations Config { get; }
        private UserRepository UserRepository { get; }
        private RefreshTokenRepository RefreshTokenRepository { get; }

        private static TimeSpan AccessTokenExpires => TimeSpan.FromSeconds(10);
        private static DateTime RefreshTokenExpires => DateTime.UtcNow.AddMinutes(5);

        public OAuthProvider(IAppConfigurations config, UserRepository userRepository, RefreshTokenRepository refreshTokenRepository)
        {
            Config = config;
            UserRepository = userRepository;
            RefreshTokenRepository = refreshTokenRepository;
        }

        public OperationResult DoPassword(JWTRequest parameters)
        {
            var user = UserRepository.GetByEmail(parameters.username);

            if (user == null || user.Password != parameters.password)
            {
                return new OperationResult
                {
                    Success = false,
                    Message = "invalid user infomation"
                };
            }

            var refreshToken = new RefreshToken
            {
                Id = Guid.NewGuid().ToString("n"),
                ExpiresUtc = RefreshTokenExpires
            };

            RefreshTokenRepository.Add(refreshToken);

            return new OperationResult<JWTResponse>
            {
                Object = GetJwt(user, refreshToken.Id)
            };
        }

        public OperationResult DoRefreshToken(JWTRequest parameters)
        {
            var token = RefreshTokenRepository.GetById(parameters.refresh_token);

            var user = UserRepository.GetByEmail(parameters.username);

            if (token == null || user == null)
            {
                return new OperationResult
                {
                    Success = false,
                    Message = "can not refresh token",
                };
            }
            else
            {
                return new OperationResult<JWTResponse>
                {
                    Object = GetJwt(user, parameters.refresh_token)
                };
            }
        }

        private JWTResponse GetJwt(User user, string refreshToken)
        {
            var now = DateTime.UtcNow;

            var claims = CreateClaims(user);

            var signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Config.Jwt.Secret));

            var jwt = new JwtSecurityToken(
                issuer: Config.Jwt.Issuer,
                audience: Config.Jwt.Audience,
                claims: claims,
                notBefore: now,
                expires: now.Add(AccessTokenExpires),
                signingCredentials: new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256));

            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            return new JWTResponse
            {
                access_token = encodedJwt,
                expires_in = (int)AccessTokenExpires.TotalSeconds,
                refresh_token = refreshToken,
                User = user
            };
        }

        private static List<Claim> CreateClaims(User user)
        {
            var claims = new List<Claim>
            {
                new Claim("UserId", user.Id.ToString()),
                new Claim(ClaimTypes.Email, user.Email)
            };

            return claims;
        }
    }
}
