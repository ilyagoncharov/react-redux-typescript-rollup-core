import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse, CancelTokenSource } from 'axios';
import { AppStore }                                                                   from '../../store/store';

export enum HttpRequestMethod
{
  Get     = 0,
  Post    = 1,
  Put     = 2,
  Delete  = 3,
  Options = 4,
  Head    = 5,
  Patch   = 6,
}

export interface HttpResult<T>
{
  httpRequest: Promise<AxiosResponse>,
  cancelTokenSource: CancelTokenSource
}

class Interceptor
{
  public constructor(private httpClient: AxiosInstance)
  {
    httpClient.interceptors.request.use(this.onFulfilled);
  }

  private onFulfilled = (config: AxiosRequestConfig) =>
  {
    const accessToken = AppStore.auth.auth ? AppStore.auth.auth.access_token : null;

    if (accessToken)
    {
      config.headers.Authorization = `Bearer ${accessToken}`;
    }

    return config;
  };
}

export class HttpServiceConfig
{
  private readonly _httpClient: AxiosInstance;

  public constructor()
  {
    const config: AxiosRequestConfig = {
      baseURL: 'http://localhost:57910'
    };

    this._httpClient = axios.create(config);

    const interceptor = new Interceptor(this._httpClient);
  }

  public makeRequest<T>(url: string, method: HttpRequestMethod, data?: any): HttpResult<T>
  {
    const cancelTokenSource: CancelTokenSource = axios.CancelToken.source();

    const requestConfig: AxiosRequestConfig = {
      url: url,
      method: HttpRequestMethod[method],
      cancelToken: cancelTokenSource.token
    };

    if (HttpRequestMethod[method] === HttpRequestMethod[HttpRequestMethod.Get])
    {
      requestConfig.params = data;
    }
    else if (HttpRequestMethod[method] === HttpRequestMethod[HttpRequestMethod.Post])
    {
      requestConfig.data = data;
    }

    return {
      httpRequest: this._httpClient.request<T>(requestConfig).then((response: AxiosResponse) => response.data),
      cancelTokenSource: cancelTokenSource
    };
  }
}
