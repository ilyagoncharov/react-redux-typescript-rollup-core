import { BrowserStorage } from '../library/common.library';

export class StorageService
{
  public static readonly VERSION = 1;

  public static readonly NAMESPACE = 'toys.';

  private static readonly _global = new BrowserStorage([StorageService.VERSION, 'global'].join(':'));

  public static get global(): BrowserStorage
  {
    return this._global;
  }
}
