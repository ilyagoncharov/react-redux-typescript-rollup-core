import { StorageService } from './storage.service';
import { Authentication } from '../models/dto.models';

export class AuthService
{
  private static readonly AUTHENTICATION = StorageService.NAMESPACE + 'authentication';

  public static get auth(): Authentication
  {
    return StorageService.global.getObject<Authentication>(AuthService.AUTHENTICATION);
  }

  public static initialize(auth: Authentication): void
  {
    StorageService.global.setObject<Authentication>(AuthService.AUTHENTICATION, auth);
  }
}
