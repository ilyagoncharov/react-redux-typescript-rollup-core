import { AxiosError } from 'axios';

export function showError(e: Error)
{
  if (e.hasOwnProperty('config'))
  {
    const axiosError = e as AxiosError;
    const res = {
      name: axiosError.name,
      message: axiosError.message,
      config: axiosError.config,
      response: axiosError.response,
      code: axiosError.code
    };
    console.error('axiosError', res);
  }
  else
  {
    console.error('error', e);
  }
}

export function getAxiosError(e: any): AxiosError
{
  if (e.hasOwnProperty('config'))
  {
    const axiosError = e as AxiosError;

    return {
      name: axiosError.name,
      message: axiosError.message,
      config: axiosError.config,
      response: axiosError.response,
      code: axiosError.code
    };
  }
  else
  {
    throw new Error(e);
  }
}