
export class OperationResult<T>
{
  public object: T;
  public success: boolean;
  public message: string;
}

export class UserDetails
{
  public id: number;
  public email: string;
  public password: string;
  public name: string;
}

export class ProjectDetails
{
  public id: number;
  public name: string;
}

type GrantType = 'password' | 'refresh_token'

export class JWTRequest
{
  public grant_type: GrantType;
  public refresh_token: string;
  public username: string;
  public password: string;
}

export class Authentication
{
  public access_token: string;
  public expires_in: number;
  public refresh_token: string;
  public user: UserDetails;
}

export class UserLogin
{
  public email: string;
  public password: string;
}
