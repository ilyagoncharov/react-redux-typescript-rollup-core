import * as React                  from 'react';
import * as ReactDOM               from 'react-dom';
import { Provider }                from 'react-redux';
import { AppConnect } from './components/application';
import { AppStore }                from './store/store';

ReactDOM.render(
    (<Provider store={AppStore.store}>
        <AppConnect/>
    </Provider>),
    document.getElementById('application')
);
