import { SagaMiddleware } from 'redux-saga';
import { authSagas }      from './authenticate/sagas';
import { userSagas }      from './user/sagas';
import { all }            from 'redux-saga/effects';

function* rootSaga()
{
  yield all([
    ...authSagas,
    ...userSagas
  ]);
}

export const sagasRun = (sagaMiddleware: SagaMiddleware<object>) =>
{
  sagaMiddleware.run(rootSaga);
};
