import { call, put, takeEvery }                                   from 'redux-saga/effects';
import { Authentication, JWTRequest, OperationResult, UserLogin } from '../../models/dto.models';
import { AuthActions, AuthActionType, AuthLogInAction }           from './actions';
import { showError }                                              from '../../library/error';
import { HttpRequestMethod }                                      from '../../services/webapi/http.service';
import { AppStore }                                               from '../store';
import { authRequestFactory }                                     from '../common/http.factory';
import { watcherFactory }                                         from '../common/sagas';

const refreshTokenWatcher = watcherFactory(AuthActionType.AUTH_REFRESH_TOKEN_REQUEST, refreshToken, 0);

const authenticateRequest = authRequestFactory<OperationResult<Authentication>, JWTRequest>('api/token', HttpRequestMethod.Post);

function* authenticate(userLogin: UserLogin)
{
  const request = new JWTRequest();
  request.grant_type = 'password';
  request.username = userLogin.email;
  request.password = userLogin.password;

  const data: OperationResult<Authentication> = authenticateRequest.call(yield call(authenticateRequest.generator, request));

  if (data.success)
  {
    yield put(AuthActions.logInSuccess(data.object));
  }
}

export function* refreshToken()
{
  const auth = AppStore.auth;

  if (!auth.auth)
  {
    AppStore.routeNavigate('/login');
  }

  console.log('refreshToken start');

  yield put(AuthActions.refreshTokenStart());

  try
  {
    const request = new JWTRequest();
    request.grant_type = 'refresh_token';
    request.username = auth.auth.user.email;
    request.refresh_token = auth.auth.refresh_token;

    const data: OperationResult<Authentication> = authenticateRequest.call(yield call(authenticateRequest.generator, request));

    if (data.success)
    {
      yield put(AuthActions.logInSuccess(data.object));
    }
    else
    {
      yield put(AuthActions.logOut());
    }

    return data;
  }
  finally
  {
    yield put(AuthActions.refreshTokenEnd());
  }
}

function* logInSaga(action: AuthLogInAction)
{
  try
  {
    yield call(authenticate, action.payload);
  }
  catch (e)
  {
    showError(e);

    yield put(AuthActions.logInError());
  }
}

function* logOutSaga()
{
  try
  {
    console.log('logOUT SAGA')
    yield put({type: ''})
  }
  catch (e)
  {

  }
}

export const authSagas = [
  takeEvery(AuthActionType.AUTH_LOG_IN, logInSaga),
  refreshTokenWatcher(),
  takeEvery(AuthActionType.AUTH_LOG_OUT, logOutSaga)
];
