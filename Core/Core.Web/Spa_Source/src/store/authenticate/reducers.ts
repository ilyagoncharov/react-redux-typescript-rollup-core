import { Authentication } from '../../models/dto.models';
import { AuthActionType } from './actions';
import { RootAction }     from '../actions';
import { AuthService }    from '../../services/auth.service';

export interface IAuthenticationState
{
  auth: Authentication;
  refreshTokenRequestActive: boolean;
}

const init: IAuthenticationState = {
  auth: AuthService.auth,
  refreshTokenRequestActive: false
};

export const authenticationReducer = (state: IAuthenticationState = init, action: RootAction): IAuthenticationState =>
{
  switch (action.type)
  {
    case AuthActionType.AUTH_LOG_IN_SUCCESS:
      AuthService.initialize(action.payload);
      return {...state, auth: action.payload};
    case AuthActionType.AUTH_LOG_IN_ERROR:
      return {...state, auth: null};
    case AuthActionType.AUTH_LOG_OUT:
      console.log('logOUT REDUCER')
      return {...state, auth: null};
    case AuthActionType.AUTH_REFRESH_TOKEN_START:
      return {...state, refreshTokenRequestActive: true};
    case AuthActionType.AUTH_REFRESH_TOKEN_END:
      return {...state, refreshTokenRequestActive: false};
    default:
      return state;
  }
};
