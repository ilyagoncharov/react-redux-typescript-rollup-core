import { actionFactory, ActionWithPayload } from '../common/actions';
import { Authentication, UserLogin }        from '../../models/dto.models';
import { Action, Dispatch }                 from 'redux';
import { AppStore }                         from '../store';

export enum AuthActionType
{
  AUTH_LOG_IN                = 'AUTH_LOG_IN',
  AUTH_LOG_IN_SUCCESS        = 'AUTH_LOG_IN_SUCCESS',
  AUTH_LOG_IN_ERROR          = 'AUTH_LOG_IN_ERROR',
  AUTH_LOG_OUT               = 'AUTH_LOG_OUT',
  AUTH_REFRESH_TOKEN_REQUEST = 'AUTH_REFRESH_TOKEN_REQUEST',
  AUTH_REFRESH_TOKEN_START   = 'AUTH_REFRESH_TOKEN_START',
  AUTH_REFRESH_TOKEN_END     = 'AUTH_REFRESH_TOKEN_END'
}

export type AuthLogInAction = ActionWithPayload<AuthActionType.AUTH_LOG_IN, UserLogin>;
export type AuthLogInSuccessAction = ActionWithPayload<AuthActionType.AUTH_LOG_IN_SUCCESS, Authentication>;
export type AuthLogInErrorAction = Action<AuthActionType.AUTH_LOG_IN_ERROR>;
export type AuthLogOutAction = Action<AuthActionType.AUTH_LOG_OUT>;

export type AuthRefreshTokenRequest = Action<AuthActionType.AUTH_REFRESH_TOKEN_REQUEST>;
export type AuthRefreshTokenStart = Action<AuthActionType.AUTH_REFRESH_TOKEN_START>;
export type AuthRefreshTokenEnd = Action<AuthActionType.AUTH_REFRESH_TOKEN_END>;


const logIn = actionFactory<AuthLogInAction>(AuthActionType.AUTH_LOG_IN);
const logInSuccess = actionFactory<AuthLogInSuccessAction>(AuthActionType.AUTH_LOG_IN_SUCCESS);
const logInError = actionFactory<AuthLogInErrorAction>(AuthActionType.AUTH_LOG_IN_ERROR);
const logOut = actionFactory<AuthLogOutAction>(AuthActionType.AUTH_LOG_OUT);

const refreshTokenRequest = actionFactory<AuthRefreshTokenRequest>(AuthActionType.AUTH_REFRESH_TOKEN_REQUEST);
const refreshTokenStart = actionFactory<AuthRefreshTokenStart>(AuthActionType.AUTH_REFRESH_TOKEN_START);
const refreshTokenEnd = actionFactory<AuthRefreshTokenEnd>(AuthActionType.AUTH_REFRESH_TOKEN_END);

function thinkLogOut()
{
  return (dispatch: Dispatch) =>
  {
    AppStore.routeNavigate('');
    dispatch(logOut);
  }
}

export const AuthActions = {logIn, logInSuccess, logInError, logOut, refreshTokenRequest, refreshTokenStart, refreshTokenEnd};
