import { applyMiddleware, compose, createStore, Dispatch, Middleware, Store } from 'redux';
import { thunk }                                                              from './redux-thunk';
import { composeWithDevTools }                                                from 'redux-devtools-extension';
import { IState, reducers }                                                   from './state';
import createSagaMiddleware                                                   from 'redux-saga';
import { sagasRun }                                                           from './sagas';
import { IAuthenticationState }                                               from './authenticate/reducers';

const sagaMiddleware = createSagaMiddleware();

const isDevelopment = () =>
{
  const nodeEnv = process.env.NODE_ENV;
  return nodeEnv === 'development';
};

const configureStore = (): Store<IState> =>
{
  const composeEnhancers = isDevelopment()
    ? composeWithDevTools({})
    : compose();

  const middlewares: Middleware[] = [
    sagaMiddleware,
    thunk
  ];

  return createStore(
    reducers,
    composeEnhancers(applyMiddleware(...middlewares))
  );
};

const store = configureStore();

export class AppStore
{
  public static get store(): Store<IState>
  {
    return store;
  }

  public static get dispatch(): Dispatch
  {
    return store.dispatch;
  }

  public static routeNavigate(url: string): void
  {
    // store.dispatch(push(url));
  }

  public static get auth(): IAuthenticationState
  {
    return store.getState().authentication;
  }
}

sagasRun(sagaMiddleware);
