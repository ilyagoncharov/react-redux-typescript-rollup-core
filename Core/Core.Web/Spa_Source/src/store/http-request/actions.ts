import { ActionHttpRequest, httpRequestFactory } from '../common/actions';

export enum HttpRequestActionType
{
  HTTP_REQUEST_ABORT   = 'HTTP_REQUEST_ABORT',
}

export type HttpRequestAbortAction = ActionHttpRequest<HttpRequestActionType.HTTP_REQUEST_ABORT>;
const abort = httpRequestFactory<HttpRequestAbortAction>(HttpRequestActionType.HTTP_REQUEST_ABORT);

export const HttpRequestActions = {abort};
