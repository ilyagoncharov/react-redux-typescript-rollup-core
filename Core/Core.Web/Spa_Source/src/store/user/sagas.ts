import { call, put }                                        from 'redux-saga/effects';
import { UserActions, UserActionType, UserGetSingleAction } from './actions';
import { UserDetails }                                      from '../../models/dto.models';
import { HttpRequestMethod }                                from '../../services/webapi/http.service';
import { httpWatcherFactory }                               from '../common/sagas';
import { httpRequestFactory }                               from '../common/http.factory';

const userListWatcher = httpWatcherFactory(UserActionType.USER_GET_LIST, userListSaga);
const userSingleWatcher = httpWatcherFactory(UserActionType.USER_GET_SINGLE, userSingleSaga);

const getUsersRequest = httpRequestFactory<UserDetails[], never>('api/data/users', HttpRequestMethod.Get);
const getUserRequest = httpRequestFactory<UserDetails, { id: number }>('api/data/user', HttpRequestMethod.Get);

function* userListSaga()
{
  try
  {
    const data: UserDetails[] = getUsersRequest.call(yield call(getUsersRequest.generator));

    yield put(UserActions.getListSuccess(data));
  }
  catch (e)
  {
    console.error('Error');
  }
}

function* userSingleSaga(action: UserGetSingleAction)
{
  try
  {
    const params = {
      id: action.payload
    };

    const data: UserDetails = getUserRequest.call(yield call(getUserRequest.generator, params));

    yield put(UserActions.getSingleSuccess(data));
  }
  catch (e)
  {

  }
}

export const userSagas = [
  userListWatcher(),
  userSingleWatcher()
];
