import { UserDetails }    from '../../models/dto.models';
import { RootAction }     from '../actions';
import { UserActionType } from './actions';

export interface IUserState
{
  users: UserDetails[];
}

const init: IUserState = {
  users: []
};

export const userReducer = (state: IUserState = init, action: RootAction): IUserState =>
{
  switch (action.type)
  {
    case UserActionType.USER_GET_LIST_SUCCESS:
      return {...state, users: [...state.users, ...action.payload]};
    case UserActionType.USER_GET_SINGLE_SUCCESS:
      return {...state, users: [...state.users, action.payload]};
    case UserActionType.USER_GET_LIST_ERROR:
    default:
      return state;
  }
};
