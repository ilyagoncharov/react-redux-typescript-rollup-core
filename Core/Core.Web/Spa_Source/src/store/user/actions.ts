import { Action }                           from 'redux';
import { actionFactory, ActionWithPayload } from '../common/actions';
import { UserDetails }                      from '../../models/dto.models';

export enum UserActionType
{
  USER_GET_LIST           = 'USER_GET_LIST',
  USER_GET_LIST_SUCCESS   = 'USER_GET_LIST_SUCCESS',
  USER_GET_SINGLE         = 'USER_GET_SINGLE',
  USER_GET_SINGLE_SUCCESS = 'USER_GET_SINGLE_SUCCESS'
}

export type UserGetListAction = Action<UserActionType.USER_GET_LIST>;
export type UserGetListSuccessAction = ActionWithPayload<UserActionType.USER_GET_LIST_SUCCESS, UserDetails[]>;

export type UserGetSingleAction = ActionWithPayload<UserActionType.USER_GET_SINGLE, number>;
export type UserGetSingleSuccessAction = ActionWithPayload<UserActionType.USER_GET_SINGLE_SUCCESS, UserDetails>;


const getList = actionFactory<UserGetListAction>(UserActionType.USER_GET_LIST);
const getListSuccess = actionFactory<UserGetListSuccessAction>(UserActionType.USER_GET_LIST_SUCCESS);

const getSingle = actionFactory<UserGetSingleAction>(UserActionType.USER_GET_SINGLE);
const getSingleSuccess = actionFactory<UserGetSingleSuccessAction>(UserActionType.USER_GET_SINGLE_SUCCESS);

export const UserActions = {getList, getListSuccess, getSingle, getSingleSuccess};
