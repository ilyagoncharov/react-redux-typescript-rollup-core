import { actionChannel, call, flush, race, take } from 'redux-saga/effects';
import { HttpRequestActionType }                  from '../http-request/actions';
import { ActionHttpRequest }                      from './actions';
import { Action }                                 from 'redux';
import { buffers }                                from 'redux-saga';

export const watcherFactory = <E extends string, A extends Action<E>>(type: E, saga: (action: A) => IterableIterator<any>, bufferLimit: number = 5) =>
  function* ()
  {
    const channel = yield actionChannel(type, buffers.sliding(bufferLimit));

    while (true)
    {
      const actionData = yield take(channel);

      yield call(saga, actionData);
    }
  };

export const httpWatcherFactory = <E extends string, A extends Action<E>>(type: E, saga: (action: A) => IterableIterator<any>, bufferLimit: number = 5) =>
  function* ()
  {
    const channel = yield actionChannel(type, buffers.sliding(bufferLimit));

    while (true)
    {
      const actionData = yield take(channel);

      const result = yield race({
        data: call(saga, actionData),
        abort: take(onCancel(type))
      });

      if (result.abort)
      {
        yield flush(channel);
      }
    }
  };

const onCancel = function (type: string): (action: ActionHttpRequest<any>) => boolean
{
  return (action: ActionHttpRequest<any>) =>
  {
    if (!action.startAction)
    {
      return false;
    }

    return action.startAction.type === type && action.type === HttpRequestActionType.HTTP_REQUEST_ABORT;
  };
};
