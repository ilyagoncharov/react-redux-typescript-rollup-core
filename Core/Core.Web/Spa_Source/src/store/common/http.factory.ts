import { Authentication, OperationResult }                  from '../../models/dto.models';
import { AxiosError, CancelTokenSource }                    from 'axios';
import { call, cancelled, put, take }                       from 'redux-saga/effects';
import { AppStore }                                         from '../store';
import { HttpRequestMethod, HttpServiceConfig }             from '../../services/webapi/http.service';
import { getAxiosError, showError }                         from '../../library/error';
import { AuthActions, AuthActionType, AuthRefreshTokenEnd } from '../authenticate/actions';
import { refreshToken }                                     from '../authenticate/sagas';

const HttpService = new HttpServiceConfig();

export interface IRequestGenerator<T, B>
{
  generator: (body?: B) => IterableIterator<any>;
  call: (args: any) => T;
}

export const httpRequestFactory = <T, B>(url: string, method: HttpRequestMethod): IRequestGenerator<T, B> =>
{
  const generator = function* (body?: B)
  {
    let cancelTokenSource: CancelTokenSource;

    try
    {
      if (AppStore.auth.refreshTokenRequestActive)
      {
        yield take(AuthActionType.AUTH_REFRESH_TOKEN_END);
      }

      const httpRequest = () =>
      {
        const httpResult = HttpService.makeRequest<T>(url, method, body);
        cancelTokenSource = httpResult.cancelTokenSource;
        return httpResult.httpRequest;
      };

      try
      {
        return yield call(httpRequest);
      }
      catch (e)
      {
        const error: AxiosError = getAxiosError(e);

        if (error.response.status === 401)
        {
          yield put(AuthActions.refreshTokenRequest());

          const test = yield take(AuthActionType.AUTH_REFRESH_TOKEN_END);
          console.log('TAKE', test);
          return yield call(httpRequest);
          // const response: OperationResult<Authentication> = yield call(refreshToken);
          //
          // if (response.success)
          // {
          //   return yield call(httpRequest);
          // }
        }
        else
        {
          showError(e);
        }

        throw new Error(e);
      }
    }
    finally
    {
      if (yield cancelled())
      {
        if (cancelTokenSource)
        {
          cancelTokenSource.cancel();
        }
      }
    }
  };

  return {
    generator: generator,
    call: a => a
  };
};

export const authRequestFactory = <T, B>(url: string, method: HttpRequestMethod): IRequestGenerator<T, B> =>
{
  const generator = function* (body?: B)
  {
    return yield call(() => HttpService.makeRequest<T>(url, method, body).httpRequest);
  };

  return {
    generator: generator,
    call: a => a
  }
};
