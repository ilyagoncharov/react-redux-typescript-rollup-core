import { combineReducers, Reducer }                    from 'redux';
import { authenticationReducer, IAuthenticationState } from './authenticate/reducers';
import { IUserState, userReducer }                     from './user/reducers';

export interface IState
{
  authentication: IAuthenticationState;
  users: IUserState;
}

export const reducers: Reducer<IState> = combineReducers<IState>({
  authentication: authenticationReducer,
  users: userReducer
});
