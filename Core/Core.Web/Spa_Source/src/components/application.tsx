import * as React                                              from 'react';
import { AppStore }                                            from '../store/store';
import { AuthActions }                                         from '../store/authenticate/actions';
import { UserActions, UserGetListAction, UserGetSingleAction } from '../store/user/actions';
import { HttpRequestActions }                                  from '../store/http-request/actions';
import { AnyAction }                                           from 'redux';
import { connect }                                             from 'react-redux';
import { IState }                                              from '../store/state';
import { UserDetails }                                         from '../models/dto.models';

export interface IProps
{
  users: UserDetails[];
}

class Application extends React.Component<IProps, object>
{
  private currentUserListAction: () => void;
  private currentUserSingleAction: UserGetSingleAction;

  public constructor(props: IProps)
  {
    super(props);
  }

  public onLogin(): void
  {
    AppStore.dispatch(AuthActions.logIn({email: 'admin@itmint.ca', password: 'admin@itmint.ca'}));
  }

  public onLogout(): void
  {
    AppStore.dispatch(AuthActions.logOut());
  }

  //------------------------------

  public onUserList(): void
  {
    this.currentUserListAction = this.sendRequest(UserActions.getList());
  }

  public onUserListCancel(): void
  {
     this.currentUserListAction();
  }

  public sendRequest(request: AnyAction): () => void
  {
    AppStore.dispatch(request);
    return () =>
    {
      AppStore.dispatch(HttpRequestActions.abort(request));
    };
  }

  //------------------------------

  public onUserSingle(id: number): void
  {
    this.currentUserSingleAction = AppStore.dispatch(UserActions.getSingle(id));
  }

  public onUserSingleCancel(): void
  {
    AppStore.dispatch(HttpRequestActions.abort(this.currentUserSingleAction))
  }

  //------------------------------

  public onUserMulti(): void
  {
    setTimeout(() => AppStore.dispatch(UserActions.getList()));
    setTimeout(() => AppStore.dispatch(UserActions.getSingle(2)));
  }

  public render()
  {
    return (
      <div>
        <div>
          <button onClick={e => this.onLogin()}>Login</button>
          <button onClick={e => this.onLogout()}>Logout</button>
        </div>
        <br/>
        <div style={{display: 'inline-grid'}}>
          <button onClick={e => this.onUserList()}>Get Users</button>
          <button onClick={e => this.onUserListCancel()}>Cancel</button>
        </div>

        <div style={{display: 'inline-grid'}}>
          <button onClick={e => this.onUserSingle(1)}>Get User id-1</button>
          <button onClick={e => this.onUserSingle(2)}>Get User id-2</button>
          <button onClick={e => this.onUserSingleCancel()}>Cancel</button>
        </div>

        <div style={{display: 'inline-grid'}}>
          <button onClick={e => this.onUserMulti()}>Get Multi</button>
        </div>

        <div className='users-container'>
          {
            this.props.users.map((user: UserDetails, i: number) =>
            {
              return <div key={i}>{i } {user.email}</div>
            })
          }
        </div>
      </div>
    );
  }
}

export const AppConnect = connect((state: IState) => state.users)(Application);
